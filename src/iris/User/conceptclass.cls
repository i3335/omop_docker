/// 
Class User.conceptclass Extends %Persistent [ ClassType = persistent, DdlAllowed, Final, Owner = {overevkina}, ProcedureBlock, SqlRowIdPrivate, SqlTableName = concept_class ]
{

Property conceptclassid As %Library.String(MAXLEN = 20) [ Required, SqlColumnNumber = 2, SqlFieldName = concept_class_id ];

Property conceptclassname As %Library.String(MAXLEN = 255) [ Required, SqlColumnNumber = 3, SqlFieldName = concept_class_name ];

Property conceptclassconceptid As %Library.Integer(MAXVAL = 2147483647, MINVAL = -2147483648) [ Required, SqlColumnNumber = 4, SqlFieldName = concept_class_concept_id ];

/// Bitmap Extent Index auto-generated by DDL CREATE TABLE statement.  Do not edit the SqlName of this index.
Index DDLBEIndex [ Extent, SqlName = "%%DDLBEIndex", Type = bitmap ];

Parameter USEEXTENTSET = 1;

Storage Default
{
<Data name="conceptclassDefaultData">
<Value name="1">
<Value>conceptclassid</Value>
</Value>
<Value name="2">
<Value>conceptclassname</Value>
</Value>
<Value name="3">
<Value>conceptclassconceptid</Value>
</Value>
</Data>
<DataLocation>^poCN.CNwO.1</DataLocation>
<DefaultData>conceptclassDefaultData</DefaultData>
<ExtentLocation>^poCN.CNwO</ExtentLocation>
<IdFunction>sequence</IdFunction>
<IdLocation>^poCN.CNwO.1</IdLocation>
<Index name="DDLBEIndex">
<Location>^poCN.CNwO.2</Location>
</Index>
<Index name="IDKEY">
<Location>^poCN.CNwO.1</Location>
</Index>
<IndexLocation>^poCN.CNwO.I</IndexLocation>
<StreamLocation>^poCN.CNwO.S</StreamLocation>
<Type>%Storage.Persistent</Type>
}

}
