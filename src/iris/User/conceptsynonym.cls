/// 
Class User.conceptsynonym Extends %Persistent [ ClassType = persistent, DdlAllowed, Final, Owner = {kchalyh}, ProcedureBlock, SqlRowIdPrivate, SqlTableName = concept_synonym ]
{

/// Property conceptsynonymid As %Library.Integer(MAXVAL = 2147483647, MINVAL = -2147483648) [ Required, SqlColumnNumber = 2, SqlFieldName = concept_synonym_id ];
Property conceptid As %Library.Integer(MAXVAL = 2147483647, MINVAL = -2147483648) [ Required, SqlColumnNumber = 3, SqlFieldName = concept_id ];

Property conceptsynonymname As %Library.String(MAXLEN = 1000) [ Required, SqlColumnNumber = 4, SqlFieldName = concept_synonym_name ];

Property languageconceptid As %Library.Integer(MAXVAL = 2147483647, MINVAL = -2147483648) [ Required, SqlColumnNumber = 5, SqlFieldName = language_concept_id ];

Parameter USEEXTENTSET = 1;

/// Bitmap Extent Index auto-generated by DDL CREATE TABLE statement.  Do not edit the SqlName of this index.
Index DDLBEIndex [ Extent, SqlName = "%%DDLBEIndex", Type = bitmap ];

Storage Default
{
<Data name="conceptsynonymDefaultData">
<Value name="1">
<Value>conceptsynonymid</Value>
</Value>
<Value name="2">
<Value>conceptid</Value>
</Value>
<Value name="3">
<Value>conceptsynonymname</Value>
</Value>
<Value name="4">
<Value>languageconceptid</Value>
</Value>
</Data>
<DataLocation>^poCN.Dtp0.1</DataLocation>
<DefaultData>conceptsynonymDefaultData</DefaultData>
<ExtentLocation>^poCN.Dtp0</ExtentLocation>
<ExtentSize>18344</ExtentSize>
<IdFunction>sequence</IdFunction>
<IdLocation>^poCN.Dtp0.1</IdLocation>
<Index name="DDLBEIndex">
<Location>^poCN.Dtp0.2</Location>
</Index>
<Index name="IDKEY">
<Location>^poCN.Dtp0.1</Location>
</Index>
<IndexLocation>^poCN.Dtp0.I</IndexLocation>
<Property name="%%ID">
<AverageFieldSize>3.99</AverageFieldSize>
<Histogram>$lb(.06666666666666666667,1,0,$lb(1,1223,2446,3669,4892,6115,7338,8561,9784,11007,12230,13453,14676,15899,17122,18344),$lb(1,0,0,0,0,0,0,0,0,1,1,1,1,1,1),$lb(822083584,0,842150656,825373235,842282038,842282038,859190841,859190841,876099890,876099890,909193525,909193525,926102328,926102328,943011377,943011377,959920180,959920180,825307184,825241655,842150704,842150704,859059507,859059507,875968310,875968310,892877113,892877113,925970994,925970994,942879796,825766708))</Histogram>
<Selectivity>1</Selectivity>
</Property>
<Property name="conceptid">
<AverageFieldSize>6</AverageFieldSize>
<Histogram>$lb(.06666666666666666667,1,0,$lb(33,41922073,41923435,41924794,41926173,41970932,41999799,42002105,42006880,42008616,42010109,42011483,42012857,42014349,42020109,45890995),$lb(0,4,4,4,3,3,1,4,4,3,4,4,4,3,1),$lb(858980352,858980352,875641138,842020659,859058997,859058997,876034356,876034356,909195059,842412343,925907251,925907251,960051001,825833785,842018866,842084405,909654064,909654064,943075638,808990257,825241904,808529977,825505843,825505843,842544439,842544439,875770937,825504564,842019120,842019376,892877104,875903033))</Histogram>
<Selectivity>0.0055%</Selectivity>
</Property>
<Property name="conceptsynonymname">
<AverageFieldSize>17.55</AverageFieldSize>
<Histogram>$lb(.06666666666666666667,0,0,$lb(" 'S-GRAVENHAGE"," SECTEUR SUD-OUEST / CONLEAU"," Ä¸"_$c(139)_"Ç"_$c(155,138)_"Å"_$c(159,142)_"É"_$c(131)_"¡"," Å"_$c(134,134)_"Å±±È¥¿Ç"_$c(148)_"º9Ä¸"_$c(129)_"Ç"_$c(155)_"®"," Å"_$c(140,151)_"39Æ"_$c(157)_"¡Æ"_$c(157)_"±1Ä¸"_$c(129)_"Ç"_$c(155)_"®"," Å"_$c(141,151)_"5Æ"_$c(157)_"¡Æ"_$c(157)_"±2Ä¸"_$c(129)_"Ç"_$c(155)_"®"," Å¤§Å °Ä¹¡"," Å±±É"_$c(131)_"½Ç"_$c(148)_"º"," Æ"_$c(150)_"°Å·"_$c(157)_"È¥¿4Æ"_$c(157)_"¡3Ä¸"_$c(129)_"Ç"_$c(155)_"®"," Æ "_$c(132)_"Ç"_$c(148)_"º3Ä¸"_$c(129)_"Ç"_$c(155)_"®"," Ç"_$c(140)_"¿Æ¥½Ç"_$c(148)_"º2"," Ç½"_$c(151)_"Å"_$c(159,142)_"Ä»«Ä½¬Æ"_$c(151,143)_"È"_$c(135)_"ªÆ²»Å"_$c(142)_"¿ / LUOCHENG"," È±"_$c(138)_"Å¹³4Æ"_$c(157)_"¡12Ä¸"_$c(129)_"Ç"_$c(155)_"®"," Éº¦ÆºªÄ¹¡"," Ì"_$c(136,152)_"Ì "_$c(149)_"4Ë"_$c(143,153)," Ù"_$c(138)_"Ø±Ù"_$c(131)_"Ø§"),$lb(1,1,1,8,9,2,2,1,2,1,2,1,1,1,1),$lb(539448109,659762503,1397048148,1397048148,3300428743,3300428743,3313927877,2780809108,2714148273,3332223281,2645635780,2375497158,2762458528,2762458528,2981218691,3316756937,3331764421,2528167351,2693056404,3332408519,3347890118,2361378469,3180840351,3351091141,3367078597,3367078597,3384452806,3384452806,3431504076,3431504076,3649755313,551127768))</Histogram>
<Selectivity>0.0056%</Selectivity>
</Property>
<Property name="languageconceptid">
<AverageFieldSize>5</AverageFieldSize>
<Histogram>$lb(.06666666666666666667,1,0,$lb(4175771,4175771,4180186,4181524,4181524,4181524,4181524,4181524,4181524,4181524,4182504,4182948,4182948,4182948,4182948,4182948),$lb(7,2,3,7,7,7,7,7,7,3,4,7,7,7,7),$lb(875640629,0,0,926234423,942682424,808532022,825569844,0,0,0,0,0,0,0,0,0,0,0,0,825569844,842346548,892351488,959723520,0,0,0,0,0,0,0,0,875640882))</Histogram>
<OutlierSelectivity>.482501:4181524</OutlierSelectivity>
<Selectivity>3.9808%</Selectivity>
</Property>
<SQLMap name="%%DDLBEIndex">
<BlockCount>-4</BlockCount>
</SQLMap>
<SQLMap name="IDKEY">
<BlockCount>-360</BlockCount>
</SQLMap>
<StreamLocation>^poCN.Dtp0.S</StreamLocation>
<Type>%Storage.Persistent</Type>
}

}
