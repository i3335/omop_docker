Class OMOP.Request.getCheckCreateCareProviderBP Extends Ens.Request
{

Parameter RESPONSECLASSNAME = "OMOP.Response.getCareProviderBO";

Property patientId As %String(MAXLEN = "", XMLNAME = "patientId");

Storage Default
{
<Data name="getCheckCreateCareProviderBPDefaultData">
<Subscript>"getCheckCreateCareProviderBP"</Subscript>
<Value name="1">
<Value>patientId</Value>
</Value>
</Data>
<DefaultData>getCheckCreateCareProviderBPDefaultData</DefaultData>
<Type>%Storage.Persistent</Type>
}

}
