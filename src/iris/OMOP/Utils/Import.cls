Class OMOP.Utils.Import [ Abstract ]
{

/// 
/// 2022 03 28 Denis Bulashev
/// Import artificial data to OMOP from csv files
/// From here http://www.ltscomputingllc.com/downloads/
/// 
/// d ##class(OMOP.Utils.Import).ImportData("/opt/irisbuild/data/")
ClassMethod ImportData(dataDir = "")
{
	try{
		q:dataDir=""
		; vocabularies
		for fileName = "CONCEPT", "CONCEPT_ANCESTOR", "CONCEPT_CLASS", "CONCEPT_RELATIONSHIP", "CONCEPT_SYNONYM", "DOMAIN", "DRUG_STRENGTH", "VOCABULARY", "RELATIONSHIP" {
			set sql = $$getInsertStatement(fileName)
			
			w "Processing file: "_dataDir_fileName_".csv",!
			set fileCls = ##class(%Library.FileCharacterStream).%New()
			set fileCls.Filename = dataDir_fileName_".csv"
			set linesCount = 0
			
			while 'fileCls.AtEnd {
				set fileLine = fileCls.ReadLine()
				set linesCount=linesCount + 1
				
				;continue
				; skip 1st row with column names
				continue:linesCount=1
				; continue:linesCount>10
				
				set rs=##class(%ResultSet).%New()
				set sc=rs.Prepare(sql)
				if $$$ISERR(sc){
					w sql,!
					w $system.Status.GetErrorText(sc),!
					d rs.Close()
					continue
				}
				
				
				if fileName = "CONCEPT" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6), $p(fileLine,$c(9),7), $tr("year-mM-dD", "yearmMdD",$p(fileLine,$c(9),8)), $tr("year-mM-dD", "yearmMdD",$p(fileLine,$c(9),9)), $p(fileLine,$c(9),10) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
					
				} elseif fileName = "CONCEPT_ANCESTOR" {
					set sc=rs.Execute($p(fileLine, ",", 1), $p(fileLine, ",", 2), $p(fileLine, ",", 3), $p(fileLine, ",", 4))
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}	
					
					
				} elseif fileName = "CONCEPT_CLASS" {
					set sc=rs.Execute($p(fileLine, ",", 1), $p(fileLine, ",", 2), $p(fileLine, ",", 3) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
					
				} elseif fileName = "CONCEPT_RELATIONSHIP" {
					set sc=rs.Execute($p(fileLine, ",", 1), $p(fileLine, ",", 2), $p(fileLine, ",", 3), $tr("year-mM-dD", "yearmMdD", $p(fileLine, ",", 4)), $tr("year-mM-dD", "yearmMdD", $p(fileLine, ",", 5)), $p(fileLine, ",", 6) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
					
				} elseif fileName = "CONCEPT_SYNONYM" {
					set sc=rs.Execute($p(fileLine, ",", 1), $p(fileLine, ",", 2), $p(fileLine, ",", 3) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
					
				} elseif fileName = "DOMAIN" {
					set sc=rs.Execute($p(fileLine, ",", 1), $p(fileLine, ",", 2), $p(fileLine, ",", 3) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
					
				} elseif fileName = "VOCABULARY" {
					set sc=rs.Execute($p(fileLine, ",", 1), $p(fileLine, ",", 2), $p(fileLine, ",", 3), $p(fileLine, ",", 4), $p(fileLine, ",", 5))
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
					
				} elseif fileName = "RELATIONSHIP" {
					set sc=rs.Execute($p(fileLine, ",", 1), $p(fileLine, ",", 2), $p(fileLine, ",", 3), $p(fileLine, ",", 4), $p(fileLine, ",", 5), $p(fileLine, ",", 6) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
					
				} else {
					w "Unknown "_fileName,!
				}
					
				d rs.Close()
				
				
			}
			
			w "Rows count: "_linesCount,!
		}


		; lookup tables
		for fileName = "1 Look-up2","SocialHistory1","Procedure_look_up1","Observation3","Observation2","Observation1","LabResult2","LabResult1","Diagnosis1",
						"Drugs2","Drugs1","Allergy3","Allergy2","Allergy1","1 Look-up3","1 Look-up1","Problem3","Problem2","Problem1" {
			
			set sql = "INSERT INTO lookUpTbl(Tbl_name,column_name,source_code,source_value,concept_id,concept_value,""domain"",Comments) VALUES (?,?,?,?,?,?,?,?)"
			
			w "Processing file: "_dataDir_fileName_".csv",!
			set fileCls = ##class(%Library.FileCharacterStream).%New()
			set fileCls.Filename = dataDir_fileName_".csv"
			set linesCount = 0
			
			while 'fileCls.AtEnd {
				set fileLine = fileCls.ReadLine()
				set linesCount=linesCount + 1
				
				; skip 1st row with column names
				continue:linesCount=1
				
				set rs=##class(%ResultSet).%New()
				set sc=rs.Prepare(sql)
				if $$$ISERR(sc){
					w sql,!
					w $system.Status.GetErrorText(sc),!
					d rs.Close()
					continue
				}
				
				
				set sc=rs.Execute($p(fileLine,";",2), $p(fileLine,";",3), $p(fileLine,";",4), $p(fileLine,";",5), $p(fileLine,";",6), $p(fileLine,";",7), $p(fileLine,";",8) )
				
				if $$$ISERR(sc){
					w $system.Status.GetErrorText(sc),!
					d rs.Close()
					continue
				}
					
				d rs.Close()
				
				
			}
			
			w "Rows count: "_linesCount,!
			
		}

		
		; artifitial dataset
		for fileName = "care_site","cdm_source","condition_era","condition_occurrence","cost","death","device_exposure","drug_era","drug_exposure","location","measurement","observation","observation_period","payer_plan_period","person","procedure_occurrence","provider","visit_occurrence"{
			w "Processing file: "_dataDir_fileName_".csv",!
			set sql = $$getInsertStatement(fileName)
			continue:sql=""
			
			set fileCls = ##class(%Library.FileCharacterStream).%New()
			set fileCls.Filename = dataDir_fileName_".csv"
			set linesCount = 0
			
			while 'fileCls.AtEnd {
				set fileLine = fileCls.ReadLine()
				set linesCount=linesCount + 1
				
				;continue:linesCount>10
				
				set rs=##class(%ResultSet).%New()
				set sc=rs.Prepare(sql)
				if $$$ISERR(sc){
					w sql,!
					w $system.Status.GetErrorText(sc),!
					d rs.Close()
					continue
				}
				
				if fileName = "care_site" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
				
				}elseif fileName = "cdm_source" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6), $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),9), $p(fileLine,$c(9),10) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
				}elseif fileName = "condition_era" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
				}elseif fileName = "condition_occurrence" {
					;for i=1:1:15 d
					;	. w $p(fileLine,$c(9),i),!
					
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6)
					, $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),10), $p(fileLine,$c(9),11), $p(fileLine,$c(9),12)
					, $p(fileLine,$c(9),13), $p(fileLine,$c(9),14), $p(fileLine,$c(9),15)
					 )	; -$p(fileLine,$c(9),9),
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
				}elseif fileName = "cost" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6)
					, $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),9), $p(fileLine,$c(9),10), $p(fileLine,$c(9),11), $p(fileLine,$c(9),12)
					, $p(fileLine,$c(9),13), $p(fileLine,$c(9),14), $p(fileLine,$c(9),15), $p(fileLine,$c(9),16), $p(fileLine,$c(9),17), $p(fileLine,$c(9),18)
					 )	; - $p(fileLine,$c(9),3),
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
				}elseif fileName = "death" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6), $p(fileLine,$c(9),7) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
				}elseif fileName = "device_exposure" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6)
					, $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),9), $p(fileLine,$c(9),10), $p(fileLine,$c(9),11), $p(fileLine,$c(9),12)
					, $p(fileLine,$c(9),13), $p(fileLine,$c(9),14), $p(fileLine,$c(9),15), $p(fileLine,$c(9),16), $p(fileLine,$c(9),17), $p(fileLine,$c(9),18)
					 )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}

				}elseif fileName = "drug_era" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6), $p(fileLine,$c(9),7) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}

				}elseif fileName = "drug_exposure" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6)
					, $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),9), $p(fileLine,$c(9),10), $p(fileLine,$c(9),11), $p(fileLine,$c(9),12)
					, $p(fileLine,$c(9),13), $p(fileLine,$c(9),14), $p(fileLine,$c(9),15), $p(fileLine,$c(9),16), $p(fileLine,$c(9),17), $p(fileLine,$c(9),18), $p(fileLine,$c(9),19), 
					$p(fileLine,$c(9),20), $p(fileLine,$c(9),21), $p(fileLine,$c(9),22), $p(fileLine,$c(9),23)
					 )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}

				
				}elseif fileName = "location" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6), $p(fileLine,$c(9),7), $p(fileLine,$c(9),8) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}

				}elseif fileName = "measurement" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), "", $p(fileLine,$c(9),6)
					, $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),9), $p(fileLine,$c(9),10), $p(fileLine,$c(9),11), $p(fileLine,$c(9),12)
					, $p(fileLine,$c(9),13), $p(fileLine,$c(9),14), $p(fileLine,$c(9),15), $p(fileLine,$c(9),16), $p(fileLine,$c(9),17), $p(fileLine,$c(9),18), $p(fileLine,$c(9),19), 
					$p(fileLine,$c(9),20)
					 )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
	
				}elseif fileName = "observation" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6)
					, $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),9), $p(fileLine,$c(9),10), $p(fileLine,$c(9),11), $p(fileLine,$c(9),12)
					, $p(fileLine,$c(9),13), $p(fileLine,$c(9),14), $p(fileLine,$c(9),15), $p(fileLine,$c(9),16), $p(fileLine,$c(9),17), $p(fileLine,$c(9),18)
					 )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
				
				}elseif fileName = "observation_period" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5) )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
				
				
				}elseif fileName = "payer_plan_period" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), "", $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), "", "", "", "", $p(fileLine,$c(9),6)
					, $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),9), $p(fileLine,$c(9),10), $p(fileLine,$c(9),11), $p(fileLine,$c(9),12)
					, $p(fileLine,$c(9),13), $p(fileLine,$c(9),14), $p(fileLine,$c(9),15), $p(fileLine,$c(9),16), $p(fileLine,$c(9),17), $p(fileLine,$c(9),18)
					, $p(fileLine,$c(9),19), $p(fileLine,$c(9),20), $p(fileLine,$c(9),21)
					
					 )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
				}elseif fileName = "person" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6)
					, $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),9), $p(fileLine,$c(9),10), $p(fileLine,$c(9),11), $p(fileLine,$c(9),12)
					, $p(fileLine,$c(9),13), $p(fileLine,$c(9),14), $p(fileLine,$c(9),15), $p(fileLine,$c(9),16), $p(fileLine,$c(9),17), $p(fileLine,$c(9),18)
					, $p(fileLine,$c(9),19)
					
					 )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
	
				}elseif fileName = "procedure_occurrence" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6)
					, $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),9), $p(fileLine,$c(9),10), $p(fileLine,$c(9),11), $p(fileLine,$c(9),12)
					, $p(fileLine,$c(9),13), $p(fileLine,$c(9),14)
					
					 )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
					
				}elseif fileName = "provider" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6)
					, $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),9), $p(fileLine,$c(9),10), $p(fileLine,$c(9),11), $p(fileLine,$c(9),12)
					, $p(fileLine,$c(9),13)
					
					 )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
				}elseif fileName = "visit_occurrence" {
					set sc=rs.Execute($p(fileLine,$c(9),1), $p(fileLine,$c(9),2), $p(fileLine,$c(9),3), $p(fileLine,$c(9),4), $p(fileLine,$c(9),5), $p(fileLine,$c(9),6)
					, $p(fileLine,$c(9),7), $p(fileLine,$c(9),8), $p(fileLine,$c(9),9), $p(fileLine,$c(9),10), $p(fileLine,$c(9),11), $p(fileLine,$c(9),12)
					, $p(fileLine,$c(9),13), $p(fileLine,$c(9),14), $p(fileLine,$c(9),15), $p(fileLine,$c(9),16), $p(fileLine,$c(9),17)
					
					 )
					if $$$ISERR(sc){
						w $system.Status.GetErrorText(sc),!
						d rs.Close()
						continue
					}
					
	
				} else {
					w fileLine,!
				}
				
				d rs.Close()
			}
			
			w "Rows count: "_linesCount,!
			
	}
		
		
	} catch e {
		s sc = e.AsStatus()
		w $system.Status.GetErrorText(sc),!
	}
getInsertStatement(table)
	set ret = ""
	
	if table = "care_site" {
		set ret = "INSERT INTO care_site(care_site_id, care_site_name, place_of_service_concept_id, location_id, care_site_source_value, place_of_service_source_value) VALUES (?,?,?,?,?,?)"
	
	} elseif table = "cdm_source" {
		set ret = "INSERT INTO cdm_source(cdm_source_name, cdm_source_abbreviation, cdm_holder, source_description, source_documentation_reference, cdm_etl_reference, source_release_date ,cdm_release_date, cdm_version, vocabulary_version) VALUES (?,?,?,?,?,?,?,?,?,?)"
	
	} elseif table = "condition_era" {
		set ret = "INSERT INTO condition_era(condition_era_id, person_id, condition_concept_id, condition_era_start_datetime, condition_era_end_datetime, condition_occurrence_count) VALUES (?,?,?,?,?,?)"
	
	} elseif table = "condition_occurrence" {
		set ret = "INSERT INTO condition_occurrence(condition_occurrence_id, person_id, condition_concept_id, condition_start_date, condition_start_datetime, condition_end_date, condition_end_datetime, condition_type_concept_id, condition_status_concept_id, stop_reason, provider_id, visit_occurrence_id, visit_detail_id, condition_source_value, condition_source_concept_id, condition_status_source_value) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
	
	} elseif table = "cost" {
		set ret = "INSERT INTO cost(cost_id, person_id, cost_event_id, cost_event_field_concept_id, cost_concept_id, cost_type_concept_id, currency_concept_id, cost, incurred_date, billed_date, paid_date, revenue_code_concept_id, drg_concept_id, cost_source_value, cost_source_concept_id, revenue_code_source_value, drg_source_value, payer_plan_period_id) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?)"
	
	} elseif table = "death" {
		set ret = "INSERT INTO death(person_id, death_date, death_datetime, death_type_concept_id, cause_concept_id, cause_source_value, cause_source_concept_id) VALUES (?,?,?,?,?, ?,?)"
	
	} elseif table = "device_exposure" {
		set ret = "INSERT INTO device_exposure(device_exposure_id, person_id, device_concept_id, device_exposure_start_date, device_exposure_start_datetime, device_exposure_end_date, device_exposure_end_datetime, device_type_concept_id, unique_device_id, quantity, provider_id, visit_occurrence_id, visit_detail_id, device_source_value, device_source_concept_id) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?)"

	} elseif table = "drug_era" {
		set ret = "INSERT INTO drug_era(drug_era_id, person_id, drug_concept_id, drug_era_start_datetime, drug_era_end_datetime, drug_exposure_count, gap_days) VALUES (?,?,?,?,?, ?,?)"
	
	} elseif table = "drug_exposure" {
		set ret = "INSERT INTO drug_exposure(drug_exposure_id, person_id, drug_concept_id, drug_exposure_start_date, drug_exposure_start_datetime, drug_exposure_end_date, drug_exposure_end_datetime, verbatim_end_date, drug_type_concept_id, stop_reason, refills, quantity, days_supply, sig, route_concept_id, lot_number, provider_id, visit_occurrence_id, visit_detail_id, drug_source_value, drug_source_concept_id, route_source_value, dose_unit_source_value) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?)"
	
	} elseif table = "location" {
		set ret = "INSERT INTO location(location_id, address_1, address_2, city, state, zip, county, location_source_value) VALUES (?,?,?,?,?, ?,?,?)"
	
	} elseif table = "measurement" {
		set ret = "INSERT INTO measurement(measurement_id, person_id, measurement_concept_id, measurement_date, measurement_datetime, measurement_time, measurement_type_concept_id, operator_concept_id, value_as_number, value_as_concept_id, unit_concept_id, range_low, range_high, provider_id, visit_occurrence_id, visit_detail_id, measurement_source_value, measurement_source_concept_id, unit_source_value, value_source_value) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?)"
	
	} elseif table = "observation" {
		set ret = "INSERT INTO observation(observation_id, person_id, observation_concept_id, observation_date, observation_datetime, observation_type_concept_id, value_as_number, value_as_string, value_as_concept_id, qualifier_concept_id, unit_concept_id, provider_id, visit_occurrence_id, visit_detail_id, observation_source_value, observation_source_concept_id, unit_source_value, qualifier_source_value) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?)"
	
	} elseif table = "observation_period" {
		set ret = "INSERT INTO observation_period(observation_period_id, person_id, observation_period_start_date, observation_period_end_date, period_type_concept_id) VALUES (?,?,?,?,?)"
	
	} elseif table = "payer_plan_period" {
		set ret = "INSERT INTO payer_plan_period(payer_plan_period_id, person_id, contract_person_id, payer_plan_period_start_date, payer_plan_period_end_date, payer_concept_id, plan_concept_id, contract_concept_id, sponsor_concept_id, stop_reason_concept_id, payer_source_value, payer_source_concept_id, plan_source_value, plan_source_concept_id, contract_source_value, contract_source_concept_id, sponsor_source_value, sponsor_source_concept_id, family_source_value, stop_reason_source_value, stop_reason_source_concept_id) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?)"
	
	} elseif table = "person" {
		set ret = "INSERT INTO person(person_id, gender_concept_id, year_of_birth, month_of_birth, day_of_birth, birth_datetime, race_concept_id, ethnicity_concept_id, location_id, provider_id, care_site_id, person_source_value, gender_source_value, gender_source_concept_id, race_source_value, race_source_concept_id, ethnicity_source_value, ethnicity_source_concept_id) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?)"
		
	} elseif table = "procedure_occurrence" {
		set ret = "INSERT INTO procedure_occurrence(procedure_occurrence_id, person_id, procedure_concept_id, procedure_date, procedure_datetime, procedure_type_concept_id, modifier_concept_id, quantity, provider_id, visit_occurrence_id, visit_detail_id, procedure_source_value, procedure_source_concept_id, modifier_source_value) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?)"
		
	} elseif table = "provider" {
		set ret = "INSERT INTO provider(provider_id, provider_name, NPI, DEA, specialty_concept_id, care_site_id, year_of_birth, gender_concept_id, provider_source_value, specialty_source_value, specialty_source_concept_id, gender_source_value, gender_source_concept_id) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?)"
		
	} elseif table = "visit_occurrence" {
		set ret = "INSERT INTO visit_occurrence(visit_occurrence_id, person_id, visit_concept_id, visit_start_date, visit_start_datetime, visit_end_date, visit_end_datetime, visit_type_concept_id, provider_id, care_site_id, visit_source_value, visit_source_concept_id, admitting_source_value, admitting_source_concept_id, discharge_to_source_value, discharge_to_concept_id, preceding_visit_occurrence_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
		
	} elseif table = "CONCEPT" {
		set ret = "INSERT INTO concept(concept_id,concept_name,domain_id,vocabulary_id,concept_class_id,standard_concept,concept_code,valid_start_date,valid_end_date,invalid_reason) VALUES (?,?,?,?,?,?,?,?,?,?)"
		
	} elseif table = "CONCEPT_ANCESTOR" {
		set ret = "INSERT INTO concept_ancestor(ancestor_concept_id,descendant_concept_id,min_levels_of_separation,max_levels_of_separation) VALUES (?,?,?,?)"
		
	} elseif table = "CONCEPT_CLASS" {
		set ret = "INSERT INTO concept_class(concept_class_id,concept_class_name,concept_class_concept_id) VALUES (?,?,?)"
		
	} elseif table = "CONCEPT_RELATIONSHIP" {
		set ret = "INSERT INTO concept_relationship(concept_id_1,concept_id_2,relationship_id,valid_start_date,valid_end_date,invalid_reason) VALUES (?,?,?,?,?,?)"
		
	} elseif table = "CONCEPT_SYNONYM" {
		set ret = "INSERT INTO concept_synonym(concept_id,concept_synonym_name,language_concept_id) VALUES (?,?,?)"
		
	} elseif table = "DOMAIN" {
		set ret = "INSERT INTO ""domain""(domain_id,domain_name,domain_concept_id) VALUES (?,?,?)"
		
	} elseif table = "DRUG_STRENGTH" {
		set ret = "INSERT INTO drug_strength(drug_concept_id,ingredient_concept_id,amount_value,amount_unit_concept_id,numerator_value,numerator_unit_concept_id,denominator_value,denominator_unit_concept_id,box_size,valid_start_date,valid_end_date,invalid_reason) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"
		
	} elseif table = "VOCABULARY" {
		set ret = "INSERT INTO vocabulary(vocabulary_id,vocabulary_name,vocabulary_reference,vocabulary_version,vocabulary_concept_id) VALUES (?,?,?,?,?)"
		
	} elseif table = "RELATIONSHIP" {
		set ret = "INSERT INTO relationship(relationship_id,relationship_name,is_hierarchical,defines_ancestry,reverse_relationship_id,relationship_concept_id) VALUES (?,?,?,?,?,?)"
	
	} else {
		set ret = ""
		
	}	
	
	
	q ret
}

/// d ##class(OMOP.Utils.Import)ImportTest3()
ClassMethod ImportTest3()
{
	s path = "/home/overevkina/import/"
	DO $SYSTEM.SQL.Schema.ImportDDL(path_"insert_metadata_act_covid_V4.sql",,"MSSQLServer")
}

/// d ##class(OMOP.Utils.Import).ImportDicts20211224()
ClassMethod ImportDicts20211224()
{
	s path = "/home/overevkina/import/lookUp_20211224/"
	; 1 create structure for dictionaries
	;DO $SYSTEM.SQL.Schema.ImportDDL(path_"0_create_vocab.ddl.sql",,"MSSQLServer")
	
	s fileCls = ##class(%Library.FileCharacterStream).%New()
	
	Set tStmnt=##class(%SQL.Statement).%New()
	
	s errorsCount = 0
	f tblName= "1 Look-up1_1.sql" , "1 Look-up2_1.sql", "1 Look-up3_1.sql", "Allergy1_1.sql", "Allergy2_1.sql", "Allergy3_1.sql", "Diagnosis1_1.sql", "Drugs1_1.sql", "Drugs1_2.sql", "Drugs2_1.sql", "LabResult1_1.sql", "LabResult2_1.sql", "Observation1_1.sql", "Observation2_1.sql", "Observation3_1.sql", "Problem1_1.sql", "Problem2_1.sql", "Problem2_2.sql", "Problem2_3.sql", "Problem2_4.sql", "Problem3_1.sql", "Procedure_look_up1_1.sql", "SocialHistory1_1.sql" {
		w "process file ",tblName,!
		s linesCount=0
		
		
		DO $SYSTEM.SQL.Schema.ImportDDL(path_tblName,,"MySQL")
	}
}

/// d ##class(OMOP.Utils.Import).ImportDicts()
ClassMethod ImportDicts()
{
	s path = "/home/overevkina/import/"
	; 1 create structure for dictionaries
	;DO $SYSTEM.SQL.Schema.ImportDDL(path_"0_create_vocab.ddl.sql",,"MSSQLServer")
	
	s fileCls = ##class(%Library.FileCharacterStream).%New()
	
	Set tStmnt=##class(%SQL.Statement).%New()
	
	s errorsCount = 0
	;f tblName="1_truncate_omop_base.sql","2_omop_base.concept_class.Table.sql","3_omop_base.domain.Table.sql","4_omop_base.vocabulary.Table.sql","5_1_omop_base.concept.Table.sql","5_2_omop_base.concept.Table.sql","6_omop_base.relationship.Table.sql","7_omop_base.concept_relationship.Table.sql","8_omop_base.concept_synonym.Table.sql","9_omop_base.drug_strength.Table.sql","10_omop_base.concept_ansestor.Table.sql" {
	f tblName="8_omop_base.concept_synonym.Table.sql","9_omop_base.drug_strength.Table.sql","10_omop_base.concept_ansestor.Table.sql" {
		w "process file ",tblName,!
		s linesCount=0
		
		
		s fileCls.Filename = path_tblName
		while 'fileCls.AtEnd {
			s fileLine = fileCls.ReadLine()
			s linesCount=linesCount + 1
			
			
			continue:$e(fileLine,1,3)["GO"
			quit:errorsCount>100
			
			; prepare string
			if tblName = "1_truncate_omop_base.sql" {
				s fileLine = $REPLACE(fileLine,"[omop_base].","")
				s fileLine = $REPLACE(fileLine,";","")
			} else {
				s fileLine = $REPLACE(fileLine,"[omop_base].","INTO ")
				s fileLine = $REPLACE(fileLine,");",")")
			}
			s fileLine = $REPLACE(fileLine,"[domain]","""domain""")
			s fileLine = $REPLACE(fileLine,"[","")
			s fileLine = $REPLACE(fileLine,"]","")
			s fileLine = $REPLACE(fileLine,"(N'","('")
			s fileLine = $REPLACE(fileLine,"', N'","', '")
			s fileLine = $REPLACE(fileLine,", N'",", '")
			
			
			Set tSQL = fileLine
			Set tSC = tStmnt.%Prepare(tSQL)
			If 'tSC {
				w !,!,"Error:",!,tSQL
				w $system.OBJ.DisplayError(tSC),!
				s errorsCount = errorsCount + 1
				continue
			}
			
			Set resultSet = tStmnt.%Execute()
			If (resultSet.%SQLCODE < 0) {
				;Throw ##class(%Exception.SQL).CreateFromSQLCODE(resultSet.%SQLCODE, resultSet.%Message)
				w !,!,"Error:",!,tSQL,!,resultSet.%SQLCODE,!,resultSet.%Message,!
				s errorsCount = errorsCount + 1
			}

			;w 2," ",fileLine,"|",tSQL,"|",!
			if linesCount#1000 = 0 w "Processed lines: ",linesCount,!
		}
		; DO $SYSTEM.SQL.Schema.ImportDDL(path_tblName,,"MSSQLServer")
		quit:errorsCount>100
		w "Total lines count: ",linesCount,!,!
	}
}

}
